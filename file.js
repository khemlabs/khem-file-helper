const fs = require('fs'),
	log = require('khem-log');

/**
 * Creates a directory
 * @param {string} path
 */
exports.createDir = (path, next) => {
	//Create uploaded image directory (hash)
	fs.mkdir(path, err => {
		if (err) return next(err);
		return next();
	});
};

/**
 * Moves a file from the source path to de destination
 * @param {string} source
 * @param {string} destination
 * @param {function} next
 */
exports.move = (source, destination, next) => fs.rename(source, destination, (err, data) => next(err));

/**
 * Unlinks a file
 * @param {path} path to unlink
 * @param {function} next
 */
exports.unlink = (path, next) => {
	try {
		fs.unlink(path);
		return next();
	} catch (err) {
		return next(err);
	}
};

exports.send = (path, type, res) => {
	fs.readFile(path, (err, data) => {
		if (err) {
			log.error(err, 'khem-file-helper/file', 'send');
			return res.status(404).send('');
		}
		res.writeHead(200, { 'Content-Type': type });
		return res.end(data, 'binary');
	});
};
