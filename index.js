const mime = require('mime');

const fileService = require('./file'),
  gm = require('gm').subClass({
    imageMagick: true
  }),
  async = require('async'),
  rimraf = require('rimraf'),
  log = require('khem-log');

/**
 * File manager
 *
 * @param {string} type ['image', 'video']
 * @param {string} destination
 * @param {string} model [optional] odm or orm model with saveFile method (must return a promise)
 * @param {string} file [optional] uploaded file
 * @param {array} validExtensions [optional] default: ['png', 'jpeg', 'jpg', 'gif', 'mp4', 'webm']
 * @param {boolean} unlink [optional] Remove raw file, default false
 * @param {array} resizes [optional] images resizes
 */
const fileHelper = function(type, to, file, model, validExtensions, unlink, resizes) {
  this.base = to;

  this.id = false;

  if (file) {
    this.from = file.path;
    this.destinationBase = `${this.base}${file.filename}/`;
    this.destination = `${this.destinationBase}raw`;
  }

  this.defaults = {
    definitionTypes: ['image', 'video'],
    fileType: 'raw',
    types: {
      image: ['png', 'jpeg', 'jpg', 'gif'],
      video: ['mp4', 'webm']
    },
    content: {
      image: 'image/png',
      video: 'video/webm'
    },
    sizes: [
      {
        width: 400,
        height: 215,
        name: 'wide'
      },
      {
        width: 240,
        height: 240,
        name: 'square'
      },
      {
        width: 50,
        height: 50,
        name: 'small'
      }
    ]
  };

  this.unlinkBoolean = unlink;

  this.type = this.defaults.definitionTypes.indexOf(type) >= 0 ? type : 'image';

  this.validExtensions =
    validExtensions == false || (Array.isArray(validExtensions) && validExtensions.length)
      ? validExtensions
      : this.defaults.types[this.type];

  this.resizes = Array.isArray(resizes) && resizes.length ? resizes : this.defaults.sizes;

  this.createPath = next => fileService.createDir(this.destinationBase, next);
  this.move = next => fileService.move(this.from, this.destination, next);
  this.unlink = next => {
    if (this.unlinkBoolean) return fileService.unlink(this.destination, next);
    return next();
  };

  this.validFormat = next => {
    value = mime.getExtension(file.mimetype);
    if (this.validExtensions.indexOf(value) >= 0) return next(`format ${value} is not supported`);
    return next();
  };

  /**
   * Rezise a image
   * @param {object} image
   * @param {string} destinationBase base path
   * @param {string} image path
   * @param {array} sizes {width: {number}, height: {number}, name: {string}}
   * @param {function} next
   */
  this.resize = next => {
    if (this.type != 'image') return next();
    async.each(
      this.resizes,
      (size, nextSize) => {
        gm(this.destination)
          .resize(size.width, size.height, '!')
          .write(`${this.destinationBase}/${size.name}`, err => nextSize(err));
      },
      err => next(err)
    );
  };

  this.save = next => {
    if (model && model.saveFile) {
      return model
        .saveFile(file)
        .then(data => {
          this.id = data._id;
          return next();
        })
        .catch(err => next(err));
    }
    this.id = file.filename;
    return next();
  };

  this.create = callback => {
    async.waterfall([this.validFormat, this.createPath, this.move, this.resize, this.unlink, this.save], err =>
      callback(err)
    );
  };

  this.delete = (req, res, cb) => {
    if (!model) {
      const path = `${this.base}${req.params.id}/`;
      return rimraf(path, () => {
        return cb ? cb() : res.status(204).send('');
      });
    }
    model
      .findOne({ _id: req.params.id })
      .then(file => {
        if (file) {
          const path = `${this.base}${file.filename}/`;
          return rimraf(path, () => {
            return cb ? cb() : res.status(204).send('');
          });
        }
        return res.status(404).send('not found');
      })
      .catch(err => {
        log.error(err, 'khem-file-helper', 'delete');
        return res.status(500).send('internal server error');
      });
  };

  this.send = (req, res) => {
    if (!model) {
      const base = `${this.base}${req.params.id}/`;
      const type = req.params.type || this.defaults.fileType;
      const path = `${base}${type}`;
      return fileService.send(path, this.defaults.content[this.type], res);
    }
    model
      .findOne({ _id: req.params.id })
      .then(file => {
        if (file) {
          const base = `${this.base}${file.filename}/`;
          const type = req.params.type || this.defaults.fileType;
          const path = `${base}${type}`;
          return fileService.send(path, file.mimetype, res);
        }
        return res.status(404).send('not found');
      })
      .catch(err => {
        log.error(err, 'khem-file-helper', 'send');
        return res.status(500).send('internal server error');
      });
  };

  this.getImageData = () => {
    return {
      id: this.id,
      filename: file.filename,
      extension: mime.getExtension(file.mimetype),
      mimetype: file.mimetype,
      size: file.size
    };
  };

  this.sendFileData = (err, req, res) => {
    if (err) {
      log.error(err, 'khem-file-helper/index', 'sendFileData');
      if (typeof err == 'string' && err.indexOf('not supported') >= 0) return res.status(400).send(err);
      return res.status(500).send(err);
    }
    return res.status(200).json(this.getImageData());
  };
};

/**
 * File manager
 *
 * @param {string} type ['image', 'video']
 * @param {string} destination
 * @param {string} model [optional] odm or orm model with saveFile method (must return a promise)
 * @param {string} file [optional] uploaded file
 * @param {array} validExtensions [optional] default: ['png', 'jpeg', 'jpg', 'gif', 'mp4', 'webm']
 * @param {boolean} unlink [optional] Remove raw file, default false
 * @param {array} resizes [optional] images resizes
 */
exports.configure = function(type, destination, model, file, validExtensions = [], unlink = false, resizes = []) {
  return new fileHelper(type, destination, model, file, validExtensions, unlink, resizes);
};
