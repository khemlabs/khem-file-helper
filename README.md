# README

Khemlabs - Kickstarter - Khem log

### What is this repository for?

This lib is intended for developers that are using the khemlabs kickstarter framework. It manages the file upload of
files and videos.

### Configure parameters

* {string} type ['image', 'video']
* {string} destination
* {string} file [optional] uploaded file
* {string} model [optional] odm or orm model with saveFile method (must return a promise)
* {array} validExtensions [optional] default: ['png', 'jpeg', 'jpg', 'gif', 'mp4', 'webm']
* {boolean} unlink [optional] Remove raw file, default false
* {array} resizes [optional] images resizes

### Howto

#### Send

```nodejs
const express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	upload = multer({ dest: __config.PATH_UPLOAD }),
	kfile = require('khem-file-helper'),
	kauth = require('khem-auth');

router.get('/:id/:type?', (req, res, next) => {
	// Send the file
	const file = kfile.configure('image', __config.PATH_IMG); // image or video
	return file.send(req, res);
});

module.exports = router;
```

#### Upload

```nodejs
const express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	upload = multer({ dest: __config.PATH_UPLOAD }),
	kfile = require('khem-file-helper'),
	kauth = require('khem-auth');

router.post('/', kauth.authenticate(['admin']), upload.single('file'), (req, res, next) => {
	// If the user has uploaded a file
	if (req.file && req.file.path) {
		// Configure the file object, create the file and return the id
		const file = kfile.configure('image', __config.PATH_IMG, req.file); // image or video
		//Create path, move file to path, resize file and return the id
		// Send the id of the file
		return file.create(err => file.sendFileData(err, req, res));
	}
	// If no file uploaded
	return res.json({ success: false });
});

module.exports = router;
```

#### Delete file

```nodejs
const express = require('express'),
	router = express.Router(),
	multer = require('multer'),
	upload = multer({ dest: __config.PATH_UPLOAD }),
	kfile = require('khem-file-helper'),
	kauth = require('khem-auth');

router.delete('/:id', (req, res, next) => {
	// Send the file
	const file = kfile.configure('image', __config.PATH_IMG);
	return file.delete(req, res);
});

module.exports = router;
```

#### DB PERSISTENCE

If a odm or orm model is received, it will call a saveFile method to save the file data in db. The model must persist
this data:

```json
{
	"filename": {
		"type": "String",
		"required": true,
		"unique": true
	},
	"mimetype": {
		"type": "String",
		"required": true
	},
	"size": {
		"type": "String",
		"required": true
	}
}
```

**saveFile** and **findOne** methods must exists

### Requirements

> Khemlabs kickstarter server

### Who do I talk to?

* dnangelus repo owner and admin
* developer elgambet and khemlabs
